sampler s0;

float4 PixelShaderFunction(float2 coords: TEXCOORD0) : COLOR0
{
    float4 color = tex2D(s0, coords);
    if (color.a == 0) {

    } else {
    	color.r = 1;
    	color.g = 1;
    	color.b = 1;
    	color.a = 1;
    }

    return color;
}

technique Technique1
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}