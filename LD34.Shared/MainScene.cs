﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Nez;
using Nez.Tiled;
using Nez.TextureAtlases;
using Nez.BitmapFonts;
using Nez.Particles;
using Microsoft.Xna.Framework.Audio;

namespace LD34.Shared
{
	public class MainScene : Scene
	{
		public enum GameState
		{
			Intro,
			Play,
			GameOver
		}

		public static MainScene instance;
		public TexturePackerAtlas spritesAtlas;
		public ParticleEmitterConfig puffConfig;
		public ParticleEmitterConfig muzzleConfig;
		public ParticleEmitterConfig blockExploConfig;
		public BitmapFont bmFont;

		public Text hpLabel;
		public Text scoreLabel;

		public FriendsSpawner friendsSpawner;
		public EnemiesSpawner enemiesSpawner;

		public Director director;

		public int score;
		public int hp;

		public List<SoundEffect> sfxOinks = new List<SoundEffect> ();
		public List<SoundEffect> sfxBigOinks = new List<SoundEffect> ();
		public List<SoundEffect> sfxHitOinks = new List<SoundEffect> ();
		public List<SoundEffect> sfxForks = new List<SoundEffect> ();

		public AudioManager audioManager;

		public Entity introScreen;

		public GameState gameState = GameState.Intro;

		public Effect flashEffect;

		public FlashingSpritesRenderer flashingSpritesRenderer;

		public Entity cursor;

		public Camera guiCamera;

		public MainScene() : base()
		{
			instance = this;
			clearColor = Color.Black;

			guiCamera = new Camera ();
			guiCamera.viewportAdapter = new PixelPerfectViewportAdapter( Core.graphicsDevice, 320, 240, PixelPerfectViewportAdapter.FixedSize.Both );

			// add a default renderer which will render everything
			addRenderer( new RenderLayerExcludeRenderer((int)(RenderTag.GUI)));
			flashingSpritesRenderer = new FlashingSpritesRenderer (camera, 0);
			addRenderer( flashingSpritesRenderer );
			addRenderer (new RenderLayerRenderer ((int)RenderTag.Particles));
			addRenderer (new RenderLayerRenderer ((int)RenderTag.GUI, guiCamera));

			camera.viewportAdapter = new PixelPerfectViewportAdapter( Core.graphicsDevice, 320, 240, PixelPerfectViewportAdapter.FixedSize.Both);
			#if !__IOS__
			//Core.setScreenSize( 240 * 2, 320 * 2 );
			#else
			#endif

			// Load the textures
			spritesAtlas = contentManager.Load<TexturePackerAtlas> ("Sprites/mainatlas_json");

			// Load the particles
			puffConfig = contentManager.Load<ParticleEmitterConfig>( "Particles/puff" );
			//muzzleConfig = contentManager.Load<ParticleEmitterConfig>( "Particles/Muzzle" );
			blockExploConfig = contentManager.Load<ParticleEmitterConfig>( "Particles/BlockExplo" );

			// Load fonts
			#if __WINDOWS__
			bmFont = contentManager.Load<BitmapFont>( "NezDefaultBMFont" );
			#else
			bmFont = contentManager.Load<BitmapFont>( "Fonts/pixelfont" );
			#endif
			// Load audio
			for (int i = 1; i < 5; i++) {
				sfxOinks.Add(contentManager.Load<SoundEffect>("Sounds/oink-0"+i));
			}
			for (int i = 1; i < 10; i++) {
				sfxBigOinks.Add(contentManager.Load<SoundEffect>("Sounds/oink-big-0"+i));
			}
			for (int i = 1; i < 6; i++) {
				sfxHitOinks.Add(contentManager.Load<SoundEffect>("Sounds/oink-hit-0"+i));
			}
			for (int i = 1; i < 8; i++) {
				sfxForks.Add(contentManager.Load<SoundEffect>("Sounds/fork-attack-0"+i));
			}

			// Load shaders
			flashEffect = contentManager.LoadEffect("Content/Effects/Flash.ogl.mgfxo");
			flashingSpritesRenderer.effect = flashEffect;

			// Load the level
			var tiledmap = contentManager.Load<TiledMap>( "Tilemaps/level1" );
			var levelEntity = createAndAddEntity<Entity>( "level" );
			var levelComponent = new LevelComponent (tiledmap);
			levelComponent.AddCollisionLayer ("fg", (int)CollisionLayer.Platforms, false);
			levelComponent.AddCollisionLayer ("invisible", (int)CollisionLayer.InvisibleWalls, false);
			levelEntity.addComponent (levelComponent);
			levelEntity.order += (int)DepthLayer.Background;

			// Spawn the spawners :)
			foreach (var objGroup in tiledmap.objectGroups) {
				if (objGroup.name == "friend_spawners") {
					foreach (var obj in objGroup.objects) {
						EntityFactory.CreateFriendSpawner (this, obj.x, obj.y);
					}
				} else if (objGroup.name == "enemy_spawners") {
					foreach (var obj in objGroup.objects) {
						EntityFactory.CreateEnemySpawner (this, obj.x, obj.y);
					}
				}
			}
				
			var healthBar = createAndAddEntity<Entity> ("health-bar");
			hpLabel = new Text (bmFont, "HP :", new Vector2 (0, 0), Color.AntiqueWhite);
			hpLabel.origin = new Vector2 (0, 0);
			hpLabel.renderLayer = (int)RenderTag.GUI;
			healthBar.addComponent(hpLabel);
			healthBar.tag = (int)EntityTag.GUI;
			healthBar.position = new Vector2 (10, 10);
			hpLabel.enabled = false;

			var scoreBar = createAndAddEntity<Entity> ("score-bar");
			scoreLabel = new Text (bmFont, "SCORE :", new Vector2 (0, 0), Color.AntiqueWhite);
			scoreLabel.renderLayer = (int)RenderTag.GUI;
			scoreLabel.origin = new Vector2 (0, 0);
			scoreBar.addComponent(scoreLabel);
			scoreBar.tag = (int)EntityTag.GUI;
			scoreBar.position = new Vector2 (10, 30);
			scoreLabel.enabled = false;

			camera.position = new Vector2 (16*4, 0);

			friendsSpawner = createAndAddEntity<FriendsSpawner> ("friends-spawner-system");
			enemiesSpawner = createAndAddEntity<EnemiesSpawner> ("enemies-spawner-system");

			audioManager = new AudioManager ();

			director = createAndAddEntity<Director> ("director");

			introScreen = EntityFactory.CreateIntroScreen (this);

			cursor = EntityFactory.CreateCursor (this);
		}
			
		public void Restart()
		{
			friendsSpawner.Reset ();
			enemiesSpawner.Reset ();
			director.Reset ();
			List<Entity> friends = entities.entitiesWithTag ((int)EntityTag.Friend);
			foreach (var friend in friends) {
				removeEntity (friend);
			}
			List<Entity> enemies = entities.entitiesWithTag ((int)EntityTag.Enemy);
			foreach (var enemy in enemies) {
				removeEntity (enemy);
			}
			enemiesSpawner.Start ();
			friendsSpawner.Start ();
		}
	}
}

