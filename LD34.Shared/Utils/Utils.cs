﻿using System;

namespace LD34.Shared
{

	public enum CollisionLayer { 
		All = -1, 
		Friends = 1 << 0,
		Platforms = 1 << 1,
		Enemies = 1 << 2,
		InvisibleWalls = 1 << 3
	};

	public enum DepthLayer {
		Background = 10,
		Friends = 20,
		Enemies = 30,
		Foreground = 100,
		Particles = 300
	}

	public enum EntityTag {
		GUI = 1,
		Movables = 2,
		FriendSpawner = 3,
		EnemySpawner = 4,
		Particles = 5,
		Friend = 6,
		Enemy = 7
	}

	public enum RenderTag {
		GUI = 1,
		Movables = 2,
		Particles = 3
	}

	public enum ParticleTag {
		Puff = 1,
		Muzzle = 2,
		BlockExplo = 3
	}

	public static class Utils
	{


		public static float Approach(float start, float end, float shift)
		{
			if (start < end) {
				return Math.Min (start + shift, end);
			} else {
				return Math.Max (start - shift, end);
			}
		}
	}
}

