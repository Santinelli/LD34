﻿using System;
using System.Collections.Generic;
using Nez;
using Microsoft.Xna.Framework;

namespace LD34.Shared
{
	public enum DirectionUnit { Top, Left, Right, Bottom };

	public static class PixelCollisions
	{

		private static Rectangle rectCheck = new Rectangle();

		public static bool Collide(float x, float y, float width, float height, DirectionUnit dir, int layerMask) {
			Rectangle r = RectCollision(x, y, width, height, dir);
			HashSet<Collider> colliders = Physics.boxcastBroadphase (r, layerMask);
			if (colliders.Count > 0)
				return true;
			
			return false;
		}


		public static Rectangle RectCollision(float x, float y, float width, float height, DirectionUnit dir) {
			float halfWidth = width/2;
			float halfHeight = height/2;
			float margin = 0.5f;
			float toX = 0;
			float toY = 0;
			float h = 0;
			float w = 0;

			if(dir == DirectionUnit.Top){
				toX = x - halfWidth+margin;
				toY = y - halfHeight - 1.0f - margin;
				h = 0.8f;
				w = halfWidth*2-(margin*2);
			}

			if(dir == DirectionUnit.Right){
				toX = x + halfWidth+margin + 1;
				toY = y - halfHeight + margin;
				h = height-(margin*2);;
				w = 0.8f;
			}

			if(dir == DirectionUnit.Bottom){
				toX = x - halfWidth+margin;
				toY = y + halfHeight + 1 + margin;
				h = 0.8f;
				w = width-(margin*2);
			}

			if(dir == DirectionUnit.Left){
				toX = x - halfWidth - margin;
				toY = y - halfHeight + margin;
				h = halfHeight*2-(margin*2);;
				w = 0.8f;
			}

			rectCheck.X = (int)toX;
			rectCheck.Y = (int)toY;
			rectCheck.Width = (int)w;
			rectCheck.Height = (int)h;
			return rectCheck;

		}

	}
}

