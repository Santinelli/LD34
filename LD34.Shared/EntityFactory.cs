﻿using System;
using Microsoft.Xna.Framework;
using Nez;
using Nez.Particles;
using Nez.Sprites;

namespace LD34.Shared
{
	public static class EntityFactory
	{
		public static Entity CreateFriend (MainScene scene, float x, float y)
		{
			var e = scene.createAndAddEntity<Entity>("friend");
			var sprite = new Sprite(scene.spritesAtlas.getSubtexture("friend1"));
			e.addComponent (sprite);
			sprite.originNormalized = new Vector2(0.5f, 0.5f);
			PhysicsComponent phc = new PhysicsComponent ();
			phc.layerMask = (int)(CollisionLayer.Platforms | CollisionLayer.InvisibleWalls);
			e.addComponent (phc);
			BoxCollider bc = new BoxCollider (-8, -8, 16, 16);
			bc.physicsLayer = (int)CollisionLayer.Friends;
			e.collider = bc;
			e.position = new Vector2(x, y);
			HealthComponent hc = new HealthComponent (4);
			e.addComponent (hc);
			InvulnerableComponent ic = new InvulnerableComponent ();
			e.addComponent (ic);
			e.addComponent (new InputComponent ());
			BirthComponent birth = new BirthComponent();
			e.addComponent(birth);
			TaggedParticleEmitter puffEmitter = new TaggedParticleEmitter (MainScene.instance.puffConfig, false, (int)ParticleTag.Puff);
			puffEmitter.renderLayer = (int)RenderTag.Particles;
			e.addComponent (puffEmitter);
			e.tag = (int)EntityTag.Friend;
			return e;
		}

		public static Entity CreateEnemy (MainScene scene, float x, float y)
		{
			var e = scene.createAndAddEntity<Entity>("enemy");
			var sprite = new Sprite(scene.spritesAtlas.getSubtexture("enemy1"));
			e.addComponent (sprite);
			sprite.originNormalized = new Vector2(0.5f, 0.5f);
			PhysicsComponent phc = new PhysicsComponent ();
			phc.layerMask = (int)(CollisionLayer.Platforms);
			e.addComponent (phc);
			BoxCollider bc = new BoxCollider (0, 0, 24, 24);
			bc.originNormalized = new Vector2 (0.5f, 0.5f);
			bc.physicsLayer = (int)CollisionLayer.Enemies;
			e.collider = bc;
			e.position = new Vector2(x, y);
			HealthComponent hc = new HealthComponent (1);
			e.addComponent (hc);
			MovementComponent move = new MovementComponent ();
			move.vxMax = 2;
			e.addComponent (move);
			e.addComponent (new InputComponent ());
			e.addComponent (new ChaseComponent ());
			TaggedParticleEmitter puffEmitter = new TaggedParticleEmitter (MainScene.instance.puffConfig, false, (int)ParticleTag.Puff);
			puffEmitter.renderLayer = (int)RenderTag.Particles;
			e.addComponent (puffEmitter);
			TaggedParticleEmitter expEmitter = new TaggedParticleEmitter (MainScene.instance.blockExploConfig, false, (int)ParticleTag.BlockExplo);
			expEmitter.renderLayer = (int)RenderTag.Particles;
			e.addComponent (expEmitter);
			e.tag = (int)EntityTag.Enemy;
			return e;
		}

		public static Entity CreateFriendSpawner (MainScene scene, float x, float y)
		{
			var e = scene.createAndAddEntity<Entity>("friend-spawner");
			e.position = new Vector2(x, y);
			e.tag = (int)EntityTag.FriendSpawner;
			return e;
		}

		public static Entity CreateEnemySpawner (MainScene scene, float x, float y)
		{
			var e = scene.createAndAddEntity<Entity>("enemy-spawner");
			e.position = new Vector2(x, y);
			e.tag = (int)EntityTag.EnemySpawner;
			return e;
		}

		public static Entity CreateScrollingText(MainScene scene, float x, float y, string text)
		{
			var e = scene.createAndAddEntity<Entity> ("scrolling-text");
			e.position = new Vector2 (x, y);
			e.addComponent(new Text(scene.bmFont, "", Vector2.Zero, Color.White));
			e.addComponent (new ScrollingTextComponent (text));
			return e;
		}

		public static Entity CreateIntroScreen (MainScene scene) 
		{
			var e = scene.createAndAddEntity<Entity> ("intro");
			e.position = new Vector2 (0, 0);
			var t = new Text (scene.bmFont, "Welcome to Forks and Porks!", new Vector2 (scene.camera.viewportAdapter.virtualWidth / 2, 20), Color.YellowGreen);
			t.originNormalized = new Vector2 (0.5f, 0.5f);
			t.renderLayer = (int)RenderTag.GUI;
			e.addComponent (t);

			t = new Text (scene.bmFont, "Keep your pigs safe and make sure\nthe evil villains do not reach them!\nClick with the left mouse button\non the villains to stop them\nfrom killing your pigs.\nOnce the pigs grow big enough\nthey will flee and be safe!", new Vector2 (scene.camera.viewportAdapter.virtualWidth / 2, 50), Color.White);
			t.originNormalized = new Vector2 (0.5f, 0.5f);
			t.horizontalOrigin = Text.HorizontalAlign.Center;
			t.renderLayer = (int)RenderTag.GUI;
			e.addComponent (t);

			Vector2[] points = new Vector2[] { new Vector2(5,5), new Vector2(scene.camera.viewportAdapter.virtualWidth-10,5), new Vector2(scene.camera.viewportAdapter.virtualWidth-10,scene.camera.viewportAdapter.virtualHeight-10), new Vector2(5, scene.camera.viewportAdapter.virtualHeight-10) };
			var p = new SimplePolygon (points, Color.DarkGray);
			p.renderLayer = (int)RenderTag.GUI;
			e.addComponent (p);


			points = new Vector2[] { new Vector2(0,0), new Vector2(100, 0), new Vector2(100,30), new Vector2(0, 30) };
			p = new SimplePolygon (points, Color.LightGray);
			p.originNormalized = new Vector2 (0.5f, 0.5f);
			p.localPosition = new Vector2 (scene.camera.viewportAdapter.virtualWidth / 2, 200);
			p.renderLayer = (int)RenderTag.GUI;
			e.addComponent (p);

			t = new Text (scene.bmFont, "Start", new Vector2 (scene.camera.viewportAdapter.virtualWidth / 2, p.localPosition.Y-10), Color.White);
			t.originNormalized = new Vector2 (0.5f, 0.5f);
			t.horizontalOrigin = Text.HorizontalAlign.Center;
			t.renderLayer = (int)RenderTag.GUI;
			e.addComponent (t);

			e.tag = (int)EntityTag.GUI;
			return e;
		}

		public static Entity CreateCursor(MainScene scene)
		{
			var e = scene.createAndAddEntity<Entity> ("cursor");
			e.position = new Vector2 (0, 0);
			var sprite = new Sprite(scene.spritesAtlas.getSubtexture("cursor"));
			sprite.renderLayer = (int)RenderTag.GUI;
			e.addComponent (sprite);
			sprite.originNormalized = new Vector2(0.5f, 0.5f);
			e.tag = (int)EntityTag.GUI;
			return e;
		}

	}
}

