﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Nez;

namespace LD34.Shared
{
	public class PixelPerfectViewportAdapter : ScalingViewportAdapter
	{
		public enum FixedSize {
			None,
			Width,
			Height,
			Both
		}

		public FixedSize fixedSize;

		public PixelPerfectViewportAdapter( GraphicsDevice graphicsDevice, int virtualWidth, int virtualHeight, FixedSize fixedSize ) : base( graphicsDevice, virtualWidth, virtualHeight )
		{
			this.fixedSize = fixedSize;
			onGraphicsDeviceReset();
		}


		protected override void onGraphicsDeviceReset()
		{
			var screenWidth = _graphicsDevice.PresentationParameters.BackBufferWidth;
			var screenHeight = _graphicsDevice.PresentationParameters.BackBufferHeight;
			int drawWidth = 0;
			int drawHeight = 0;
			int x = 0;
			int y = 0;

			if (fixedSize == FixedSize.Both) {
				if (screenWidth / virtualWidth > screenHeight / virtualHeight) {
					drawWidth = virtualWidth * (int)(screenHeight / virtualHeight);
					drawHeight = virtualHeight * (int)(screenHeight / virtualHeight);
				} else {
					drawWidth = virtualWidth * (int)(screenWidth / virtualWidth);
					drawHeight = virtualHeight * (int)(screenWidth / virtualWidth);
				}


				x = (int)(screenWidth / 2) - (drawWidth / 2);
				y = (int)(screenHeight / 2) - (drawHeight / 2);
			} else if (fixedSize == FixedSize.Width) {
				if( screenWidth / virtualWidth > screenHeight / virtualHeight )
				{
					drawWidth = virtualWidth * (int)( screenHeight / virtualHeight );
					drawHeight = screenHeight;
				}
				else
				{
					drawWidth = virtualWidth * (int)(screenWidth / virtualWidth);
					drawHeight = screenHeight;
				}


				x = (int)( screenWidth / 2 ) - ( drawWidth / 2 );
				y = 0;
			} else if (fixedSize == FixedSize.Height) {
				if( screenWidth / virtualWidth > screenHeight / virtualHeight )
				{
					drawWidth = screenWidth;
					drawHeight = (int)(virtualHeight * (screenHeight / virtualHeight));
				}
				else
				{
					drawWidth = screenWidth;
					drawHeight = (int)(virtualHeight * (screenHeight / virtualWidth));
				}


				x = 0;
				y = (int)(screenHeight / 2) - (drawHeight / 2);
			}

			viewport = new Viewport (x, y, drawWidth, drawHeight);

			scaleMatrix = Matrix.CreateScale (drawWidth / (float)virtualWidth);
		}


		public override Vector2 pointToVirtualViewport( Vector2 point )
		{
			point.X -= viewport.X;
			point.Y -= viewport.Y;

			return point;
		}


		public override Vector2 screenToVirtualViewport( Vector2 point )
		{
			point.X += viewport.X;
			point.Y += viewport.Y;

			return point;
		}
	}
}

