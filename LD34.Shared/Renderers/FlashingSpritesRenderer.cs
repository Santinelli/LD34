﻿using System;
using System.Collections.Generic;

using Nez;

using Microsoft.Xna.Framework.Graphics;

namespace LD34.Shared
{
	public class FlashingSpritesRenderer : Renderer
	{
		public List<RenderableComponent> flashingSprites = new List<RenderableComponent>();

		public FlashingSpritesRenderer( Camera camera, int renderOrder ) : base(camera, renderOrder)
		{
		}

		public override void render (Scene scene, bool shouldDebugRender)
		{
			var cam = camera ?? scene.camera;
			beginRender( cam );

			for( var i = 0; i < flashingSprites.Count; i++ )
			{
				var renderable = flashingSprites[i];
				if( renderable.enabled )
					renderable.render( Graphics.instance, cam );
			}

			if( shouldDebugRender )
				debugRender( scene );

			endRender();

		}

		public void add(RenderableComponent c) 
		{
			flashingSprites.Add (c);
			Game1.schedule (0.5f, false, c, onTime);
		}

		public void onTime(ITimer timer)
		{
			RenderableComponent c = timer.getContext<RenderableComponent> ();
			flashingSprites.Remove (c);
		}
	}
}

