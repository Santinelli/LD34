﻿using System;
using System.Collections;
using System.Collections.Generic;
using Nez;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace LD34.Shared
{
	public class ChaseComponent : Component
	{
		public Entity target;

		InputComponent input;

		public ChaseComponent () : base()
		{
		}

		public override void onAwake ()
		{
			base.onAwake ();
			input = entity.getComponent<InputComponent> ();
		}

		public override void update ()
		{
			base.update ();
			FindTarget ();
			Chase ();
			Attack ();
		}

		public void FindTarget() {
			if (target != null)
				return;

			List<Entity> friends = MainScene.instance.entities.entitiesWithTag((int)EntityTag.Friend);
			float minDistance = 9999999999f;
			Entity closest = null;
			foreach (var friend in friends) {
				HealthComponent hc = friend.getComponent<HealthComponent> ();
				float d = Vector2.Distance (friend.position, entity.position);
				if (d < minDistance && hc != null && hc.health > 0) {
					minDistance = d;
					closest = friend;
				}
			}
			target = closest;
		}

		public void Chase() {
			input.left = false;
			input.right = false;

			if (target == null)
				return;

			if (target.position.X > entity.position.X) {
				input.right = true;
			} else {
				input.left = true;
			}
		}

		public void Attack() {
			if (target == null)
				return;

			MovementComponent targetMove = target.getComponent<MovementComponent> ();
			float scale = 1;
			if (targetMove != null) {
				scale = targetMove.defaultScale;
			}
			if (Vector2.Distance (entity.position, target.position) < 8f * scale) {
				HealthComponent hc = target.getComponent<HealthComponent> ();
				InvulnerableComponent inv = target.getComponent<InvulnerableComponent> ();
				if (hc != null && inv == null && hc.health > 0) {
					hc.Damage (1, entity, true);
					SoundEffect se = MainScene.instance.sfxHitOinks [Nez.Random.range (0, MainScene.instance.sfxHitOinks.Count)];
					MainScene.instance.audioManager.Play (se);
				}
			}

			target = null;
		}
	}
}

