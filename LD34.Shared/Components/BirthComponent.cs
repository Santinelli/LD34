﻿using System;
using Nez;
using Microsoft.Xna.Framework;
using Nez.Sprites;

namespace LD34.Shared
{
	public class BirthComponent : Component
	{
		public float birthTime = 2f;
		public float birthCounter = 0;
		public Vector2 startPosition;
		public Vector2 endPosition;

		Sprite image;
		InvulnerableComponent inv;

		public BirthComponent () : base()
		{
		}

		public override void onAwake ()
		{
			base.onAwake ();
			image = entity.getComponent<Sprite> ();
			inv = entity.getComponent<InvulnerableComponent> ();

			startPosition = entity.position;
			endPosition = new Vector2 (entity.position.X, entity.position.Y - 16);
		}

		public override void update ()
		{
			base.update ();
			birthCounter += Time.deltaTime;
			float y = Mathf.lerp(startPosition.Y, endPosition.Y, birthCounter/birthTime);
			entity.position = new Vector2 (entity.position.X, y);
			image.rotation += Nez.Random.range (-0.05f, 0.05f);


			if (birthCounter >= birthTime) {
				entity.position = new Vector2 (entity.position.X, entity.position.Y - Nez.Random.range(16, 32));
				image.rotation = 0;
				entity.addComponent (new MovementComponent ());
				entity.addComponent (new BounceComponent ());
				entity.removeComponent (this);
				entity.removeComponent (inv);
			}
		}
	}
}

