﻿using System;
using Nez;

namespace LD34.Shared
{
	public class DelayedRemoveComponent : Component
	{
		public float time;
		public float timeCounter;

		public DelayedRemoveComponent (float time) : base()
		{
			this.time = time;
		}

		public override void update ()
		{
			base.update ();
			timeCounter += Time.deltaTime;
			if (timeCounter > time) {
				entity.scene.removeEntity (entity);
			}
		}
	}
}

