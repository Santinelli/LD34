﻿using System;
using Nez;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Nez.Sprites;

namespace LD34.Shared
{
	public class BounceComponent : Component
	{
		float movementChance = 0.3f;
		float jumpChance = 0.1f;
		float slowDown = 10;
		float counter = 0;

		float growRate = 5f; // how long they take to grow up
		float growCounter = 0;

		InputComponent input;
		Sprite image;
		MovementComponent move;
		PhysicsComponent phy;
		HealthComponent health;


		public BounceComponent () : base()
		{
		}

		public override void onAwake ()
		{
			base.onAwake ();
			input = entity.getComponent<InputComponent> ();
			image = entity.getComponent<Sprite> ();
			move = entity.getComponent<MovementComponent> ();
			phy = entity.getComponent<PhysicsComponent> ();
			health = entity.getComponent<HealthComponent> ();
		}

		public override void update ()
		{
			base.update ();

			growCounter += Time.deltaTime;
			if (growCounter >= growRate && phy.bottomCollided) {
				move.defaultScale = 2;
				//phy.vx = 0;
				//phy.vy = 0;
				input.left = false;
				input.right = false;
				input.jump = false;
				entity.position = new Vector2(entity.position.X, entity.position.Y - Nez.Random.range(32, 64));
				entity.collider.height *= 2;
				entity.collider.width *= 2;
				entity.collider.origin = new Vector2 (8, 8f);
				move.vxMax = 1;
				move.vyMax = 5;
				entity.addComponent (new FleeComponent());
				entity.removeComponent (this);
				return;
			}

			counter++;
			if (counter < slowDown)
				return;

			counter = 0;

			input.left = false;
			input.right = false;
			input.jump = false;

			if (!health.stunned) {
				if (Nez.Random.nextFloat () < movementChance) {
					SoundEffect se = MainScene.instance.sfxOinks [Nez.Random.range (0, MainScene.instance.sfxOinks.Count)];
					MainScene.instance.audioManager.Play (se);
					if (Nez.Random.nextFloat () < 0.5f) {
						input.left = true;
					} else {
						input.right = true;
					}
				}
				if (Nez.Random.nextFloat () < jumpChance) {
					input.jump = true;
				}
			}
		}
	}
}

