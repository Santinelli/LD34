﻿using System;
using Nez;

namespace LD34.Shared
{
	public class InputComponent : Component
	{
		public bool left;
		public bool right;
		public bool jump;
		public bool down;
		public bool fire;

		public bool wasLeft;
		public bool wasRight;
		public bool wasJump;
		public bool wasDown;
		public bool wasFire;

		public InputComponent () : base()
		{
		}

		public override void update ()
		{
			base.update ();
			wasLeft = left;
			wasRight = right;
			wasJump = jump;
			wasDown = down;
			wasFire = fire;
		}
	}
}

