﻿using System;
using Nez.Sprites;
using Nez.Textures;
using Microsoft.Xna.Framework.Graphics;

namespace LD34.Shared
{
	public class SpriteExComponent : Sprite
	{
		public Effect effect;

		public SpriteExComponent (Subtexture subtexture) : base(subtexture)
		{
		}

		public override void render (Nez.Graphics graphics, Nez.Camera camera)
		{
			if( subtexture != null && camera.bounds.Intersects( bounds ) )
				graphics.spriteBatch.Draw( subtexture, renderPosition, subtexture.sourceRect, color, rotation, origin, scale, spriteEffects, _layerDepth );
		}
	}
}

