﻿using System;
using System.Collections.Generic;
using Nez;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace LD34.Shared
{
	public class FleeComponent : Component
	{
		int fleeDirection = 0;
		Entity closestEnemy;
		InputComponent input;
		PhysicsComponent phy;

		float safeRate = 3f;
		float safeCounter = 0;
		bool safeDone;

		float shakeRate = 1f;
		float shakeCounter = 0;
		bool shakeDone;
		Vector2 _shakeDirection;
		Vector2 _shakeOffset;
		float _shakeIntensity = 0f;
		float _shakeDegredation = 0.95f;

		bool flyDone;

		public FleeComponent () : base()
		{
		}

		public override void onAwake ()
		{
			base.onAwake ();
			input = entity.getComponent<InputComponent> ();
			phy = entity.getComponent<PhysicsComponent> ();
		}

		public override void update ()
		{
			base.update ();
			FindClosestEnemy ();
			Flee ();
			Safe ();
			Shake ();
			FlyAway ();
		}

		public void FindClosestEnemy() {
			List<Entity> enemies = MainScene.instance.entities.entitiesWithTag((int)EntityTag.Enemy);
			float minDistance = 9999999999f;
			Entity closest = null;
			foreach (var enemy in enemies) {
				HealthComponent hc = enemy.getComponent<HealthComponent> ();
				float d = Vector2.Distance (enemy.position, entity.position);
				if (d < minDistance && hc != null && hc.health > 0) {
					minDistance = d;
					closest = enemy;
				}
			}
			closestEnemy = closest;

			if (closestEnemy != null) {
				if (closestEnemy.position.X > entity.position.X) {
					fleeDirection = -1;
				} else {
					fleeDirection = 1;
				}
			} else {
				fleeDirection = 0;
			}

		}

		public void Flee()
		{
			input.left = false;
			input.right = false;

			if (fleeDirection > 0) {
				input.right = true;
			} else if (fleeDirection < 0) {
				input.left = true;
			}

			if (Nez.Random.nextFloat () < 0.05f) {
				SoundEffect se = MainScene.instance.sfxBigOinks [Nez.Random.range (0, MainScene.instance.sfxBigOinks.Count)];
				MainScene.instance.audioManager.Play (se);
			}
		}

		public void Safe()
		{
			if (safeDone)
				return;
			
			safeCounter += Time.deltaTime;
			if (safeCounter >= safeRate) {
				safeDone = true;
				_shakeIntensity = 5f;
				entity.position = new Vector2 (entity.position.X, entity.position.Y - 20);
				entity.removeComponent (phy);
			}
		}

		public void Shake()
		{
			if (!safeDone)
				return;
			
			if (shakeDone)
				return;

			shakeCounter += Time.deltaTime;
			if (shakeCounter >= shakeRate) {
				shakeDone = true;
			}

			if( Math.Abs( _shakeIntensity ) > 0f )
			{
				_shakeOffset = _shakeDirection;
				if( _shakeOffset.X != 0f || _shakeOffset.Y != 0f )
				{
					_shakeOffset.Normalize();
				}
				else
				{
					_shakeOffset.X = _shakeOffset.X + Nez.Random.nextFloat() - 0.5f;
					_shakeOffset.Y = _shakeOffset.Y + Nez.Random.nextFloat() - 0.5f;
				}

				// TODO: this needs to be multiplied by camera zoom so that less shake gets applied when zoomed in
				_shakeOffset *= _shakeIntensity;
				_shakeIntensity *= -_shakeDegredation;
				if( Math.Abs( _shakeIntensity ) <= 0.01f )
				{
					_shakeIntensity = 0f;
				}
			}

			entity.position += _shakeOffset;

		}

		public void FlyAway() 
		{
			if (!safeDone || !shakeDone)
				return;

			if (flyDone)
				return;
			
			phy.vy = 20;
			entity.addComponent (phy);
			entity.addComponent (new DelayedRemoveComponent (2f));
			MainScene.instance.score += 10;
			flyDone = true;
			EntityFactory.CreateScrollingText (MainScene.instance, entity.position.X, entity.position.Y-16, "+10");
		}
	}
}

