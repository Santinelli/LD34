﻿using System;
using Nez.Tiled;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using Nez;


namespace LD34.Shared
{

	public class TiledCollisionLayer
	{
		public TiledTileLayer tiledTileLayer;
		public int layerMask;
		public List<Collider> colliders = new List<Collider>();
	}

	public class LevelComponent : RenderableComponent
	{
		public TiledMap tiledmap;

		public override float width
		{
			get { return tiledmap.width; }
		}

		public override float height
		{
			get { return tiledmap.height; }
		}

		List<TiledCollisionLayer> collisionLayers = new List<TiledCollisionLayer>();


		public LevelComponent( TiledMap tiledmap, string collisionLayerName = null )
		{
			this.tiledmap = tiledmap;

			Debug.warnIf( tiledmap.renderOrder != TiledRenderOrder.RightDown, "The TiledMap render order is not RightDown. Bad things might happen because of that." );
		}

		public void AddCollisionLayer(string layerName, int layerMask, bool reduceColliders)
		{
			TiledCollisionLayer cl = new TiledCollisionLayer ();
			cl.tiledTileLayer = tiledmap.getLayer<TiledTileLayer> (layerName);
			cl.layerMask = layerMask;
			collisionLayers.Add(cl);
			CreateColliders (cl, reduceColliders);
		}

		public TiledTile GetTileAtWorldPosition( Vector2 worldPos, string layerName )
		{
			// offset the passed in world position to compensate for the entity position
			worldPos -= localPosition;
			return tiledmap.getLayer<TiledTileLayer> (layerName).getTileAtWorldPosition( worldPos );
		}


		public List<TiledTile> getTilesIntersectingBounds( Rectangle bounds, string layerName)
		{
			// offset the passed in world position to compensate for the entity position
			bounds.Location -= localPosition.ToPoint();
			return tiledmap.getLayer<TiledTileLayer> (layerName).getTilesIntersectingBounds( bounds );
		}

		public void RemoveTileAtWorldPosition(Vector2 worldPos, string layerName)
		{
			worldPos -= localPosition;
			var layer = tiledmap.getLayer<TiledTileLayer> (layerName);
			var tile = layer.getTileAtWorldPosition (worldPos);
			if (tile != null) {
				layer.tiles [tile.y * layer.width + tile.x] = null;
			}
		}

		public void CreateColliders(TiledCollisionLayer collisionLayer, bool reduceColliders)
		{
			// fetch the collision layer and its rects for collision
			var collisionRects = collisionLayer.tiledTileLayer.getCollisionRectangles();

			// create colliders for the rects we received
			//collisionLayer.colliders = new Collider[collisionRects.Count];
			for( var i = 0; i < collisionRects.Count; i++ )
			{
				var collider = new BoxCollider( collisionRects[i].X, collisionRects[i].Y, collisionRects[i].Width, collisionRects[i].Height );
				collider.physicsLayer = collisionLayer.layerMask;
				collisionLayer.colliders.Add(collider);
			}
		}

		public void CreateAllColliders() 
		{
			foreach (var cl in collisionLayers) {
				CreateColliders (cl, true);
			}
		}


		public void AddColliders(List<Collider> colliders) 
		{
			foreach (var collider in colliders) {
				collider.entity = entity;
				Physics.addCollider (collider);
			}
		}

		public void RemoveColliders(List<Collider> colliders) 
		{
			foreach (var collider in colliders) {
				if (collider.entity != null) {
					Physics.removeCollider (collider, true);
				}
			}
		}

		public void UpdateColliders(TiledCollisionLayer collisionLayer)
		{
			RemoveColliders (collisionLayer.colliders);
			AddColliders (collisionLayer.colliders);
		}

		public void UpdateAllColliders()
		{
			foreach (var cl in collisionLayers) {
				RemoveColliders (cl.colliders);
				AddColliders (cl.colliders);
			}
		}

		public void DestroyAllColliders() 
		{
			foreach(var cl in collisionLayers) {
				RemoveColliders (cl.colliders);
				cl.colliders.Clear ();
			}
		}

		#region Component overrides

		public override void onAddedToEntity()
		{
			foreach (var cl in collisionLayers) {
				UpdateColliders (cl);
			}
		}


		public override void onRemovedFromEntity()
		{
			foreach (var cl in collisionLayers) {
				RemoveColliders (cl.colliders);
			}
		}


		public override void render( Graphics graphics, Camera camera )
		{
			tiledmap.draw( graphics.spriteBatch, renderPosition, layerDepth, camera.bounds );
		}


		public override void debugRender( Graphics graphics )
		{
			foreach( var group in tiledmap.objectGroups )
				renderObjectGroup( group, graphics );

			foreach (var cl in collisionLayers) {
				if( cl.colliders != null )
				{
					foreach( var collider in cl.colliders )
						collider.debugRender( graphics );
				}
			}
		}

		#endregion


		#region Rendering helpers

		void renderObjectGroup( TiledObjectGroup group, Graphics graphics )
		{
			foreach( var obj in group.objects )
			{
				if( !obj.visible )
					continue;

				switch( obj.tiledObjectType )
				{
					case TiledObject.TiledObjectType.Ellipse:
						graphics.drawCircle( new Vector2( renderPosition.X + obj.x + obj.width * 0.5f, renderPosition.Y + obj.y + obj.height * 0.5f ), obj.width * 0.5f, Color.Black );
						break;
					case TiledObject.TiledObjectType.Image:
						throw new NotImplementedException( "Image layers are not yet supported" );
					case TiledObject.TiledObjectType.Polygon:
						graphics.drawPoints( renderPosition, obj.polyPoints, Color.Black, true );
						break;
					case TiledObject.TiledObjectType.Polyline:
						graphics.drawPoints( renderPosition, obj.polyPoints, Color.Black, false );
						break;
					case TiledObject.TiledObjectType.None:
						graphics.drawHollowRect( renderPosition.X + obj.x, renderPosition.Y + obj.y, obj.width, obj.height, Color.Wheat );
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		#endregion

	}
}

