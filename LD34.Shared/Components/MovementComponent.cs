﻿using System;
using System.Collections.Generic;
using System.Collections;
using Nez;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using Nez.Particles;
using System.Linq;
using Nez.Sprites;

namespace LD34.Shared
{
	public class MovementComponent : Component
	{
		public int facing = 1;
		// Movement
		public float groundAccel = 1.00f;
		public float groundFric  = 3.00f;
		public float airAccel    = 0.75f;
		public float airFric     = 0.53f;
		public float vxMax       = 4.0f;
		public float vyMax       = 14.0f;
		public float jumpHeight  = 12.00f;
		public float gravNorm    = -0.76f;
		public float gravSlide   = -0.55f;

		public float defaultScale = 1;

		// States
		int IDLE = 10;
		int RUN = 11;
		int JUMP = 12;

		int state;

		// Legacy code
		bool sticking = false;
		bool canStick = false;

		bool wasJump = false;
		bool doubleJumped = false;

		int maxAmmo = 4;
		int currentAmmo = 4;
		float fireDelay = 0;
		float fireRate = 0.15f;
		float spread = 0.5f;

		PhysicsComponent physicsComponent;
		Sprite image;
		InputComponent input;
		TaggedParticleEmitter puffEmitter;
		TaggedParticleEmitter muzzleEmitter;

		public MovementComponent () : base()
		{
		}

		public override void onAwake ()
		{
			base.onAwake ();

			PhysicsComponent phc = entity.getComponent<PhysicsComponent> ();
			if (phc != null)
				physicsComponent = phc;
		
			Sprite img = entity.getComponent<Sprite> ();
			if (img != null)
				image = img;
		

			TaggedParticleEmitter pe = entity.components.OfType<TaggedParticleEmitter> ().FirstOrDefault(f => f.tag == (int)ParticleTag.Muzzle);
			if (pe != null) {
				muzzleEmitter = pe;
			}

			pe = entity.components.OfType<TaggedParticleEmitter> ().FirstOrDefault(f => f.tag == (int)ParticleTag.Puff);
			if (pe != null) {
				puffEmitter = pe;
			}

			input = entity.getComponent<InputComponent> ();
			
		}
		public override void update ()
		{
			base.update ();

			if (physicsComponent.bottomCollided && !physicsComponent.onGroundPrev) {
				// Squash + stretch
				physicsComponent.scale.X = 1.5f;
				physicsComponent.scale.Y = 0.5f;
			}

			/// Movement

			// Input //////////////////////////////////////////////////////////////////////

			#if __IOS__
			TouchCollection coll = Input.currentTouches;
			Rectangle bottomRight = new Rectangle(640-100, 1136-100, 100, 100);
			Rectangle bottomLeft = new Rectangle(0, 1136-100, 100, 100);
			Rectangle almostBottomLeft = new Rectangle(100, 1136-100, 100, 100);

			bool kJump = false;
			bool kJumpRelease = false;
			bool kLeft = false;
			bool kRight = false;

			foreach(var c in coll) {
				kJump = bottomRight.Contains(c.Position) && !wasJump;
				kJumpRelease = !bottomRight.Contains(c.Position) && wasJump;
				kLeft = bottomLeft.Contains(c.Position);
				kRight = almostBottomLeft.Contains(c.Position);
			}

			if(kRight)kLeft = false;

			//bool kUp = Input.GetKey(KeyCode.UpArrow);
			bool kDown = false;//(InputManager::getInstance()->isKeyPressed(sf::Keyboard::Down));

			#else
			bool kLeft = input.left;
			bool kRight = input.right;
			if(kRight)kLeft = false;

			//bool kUp = Input.GetKey(KeyCode.UpArrow);
			bool kDown = false;//(InputManager::getInstance()->isKeyPressed(sf::Keyboard::Down));

			bool kJump = input.jump && !wasJump; // check that jump has just been pressed
			bool kJumpRelease = !input.jump && wasJump; // check that jump has just been released
			#endif
			wasJump = kJump;

			bool kFire = false;//Fire();
			// Movement ///////////////////////////////////////////////////////////////////

			// Apply the correct form of acceleration and friction
			float tempAccel, tempFric;

			if (physicsComponent.bottomCollided) {
				tempAccel = groundAccel;
				tempFric  = groundFric;
			} else {
				tempAccel = airAccel;
				tempFric  = airFric;
			}

			// Reset ammo
			if (physicsComponent.bottomCollided) {
				currentAmmo = maxAmmo;
			}

			// Smoke on land
			if (physicsComponent.bottomCollided && !physicsComponent.onGroundPrev) {
				puffEmitter.emit (10); 
			}

			// Wall cling to avoid accidental push-off
			/*if ((!physicsComponent.rightCollided && !physicsComponent.leftCollided) || physicsComponent.bottomCollided) {
				canStick = true;
				sticking = false;
			} else if (((kRight && physicsComponent.leftCollided) || (kLeft && physicsComponent.rightCollided)) && canStick && !physicsComponent.bottomCollided) {
				sticking = true;
				canStick = false;
			}

			if ((kRight || kLeft) && sticking) {
				canStick = true;
				sticking = false;
			}*/


			// Handle gravity
			if (!physicsComponent.bottomCollided) {
				/*if ((physicsComponent.leftCollided || physicsComponent.rightCollided) && physicsComponent.vy < 0) {
					// Wall slide
					if(physicsComponent.onLeftPrev) physicsComponent.vy = 0;
					if(physicsComponent.onRightPrev) physicsComponent.vy = 0;

					physicsComponent.vy = Utils.Approach(physicsComponent.vy, vyMax, gravSlide);
				} else*/ {
					// Fall normally
					physicsComponent.vy = Utils.Approach(physicsComponent.vy, vyMax, gravNorm);
				}
			}


			// Left
			if (kLeft && !kRight && !sticking)
			{
				facing = -1;
				state  = RUN;
				physicsComponent.vx = Utils.Approach(physicsComponent.vx, -vxMax, tempAccel);
				//Log::log("---------------------vx "+std::to_string(baseUnit->vx));
				// Right
			} else if (kRight && !kLeft && !sticking) {
				facing = 1;
				state  = RUN;
				physicsComponent.vx = Utils.Approach(physicsComponent.vx, vxMax, tempAccel);
				//Log::log("---------------------vx "+std::to_string(baseUnit->vx));
			}

			// Friction
			if (!kRight && !kLeft && physicsComponent.vx != 0) {
				physicsComponent.vx = Utils.Approach(physicsComponent.vx, 0, tempFric);
				state = IDLE;
			}

			// Wall jump
			float jumpHeightStickY = jumpHeight * 1.1f;
			if (kJump && physicsComponent.leftCollided && !physicsComponent.bottomCollided) {
				physicsComponent.scale.X = 0.5f;
				physicsComponent.scale.Y = 1.5f;
				// Wall jump is different when pushing off/towards the wall
				if (kLeft) {
					physicsComponent.vx = jumpHeight * 0.75f;
					physicsComponent.vy = jumpHeightStickY;
				} else {
					physicsComponent.vx = vxMax;
					physicsComponent.vy = jumpHeightStickY;
				}
			} else if (kJump && physicsComponent.rightCollided && !physicsComponent.bottomCollided) {
				physicsComponent.scale.X = 0.5f;
				physicsComponent.scale.Y = 1.5f;
				// Wall jump is different when pushing off/towards the wall
				if (kRight) {
					physicsComponent.vx = -jumpHeight * 0.75f;
					physicsComponent.vy = jumpHeightStickY;
				} else {
					physicsComponent.vx = -vxMax;
					physicsComponent.vy = jumpHeightStickY;
				}
			}
			if(kFire){
				physicsComponent.vy = 0;
				physicsComponent.scale.X = 0.5f;
				physicsComponent.scale.Y = 1.5f;
			}

			// Jump 
			if (kJump) { 
				if (physicsComponent.bottomCollided) {
					doubleJumped = false;
					physicsComponent.scale.X = 0.5f;
					physicsComponent.scale.Y = 1.5f;
					physicsComponent.vy = jumpHeight;
				} else if (!doubleJumped && !physicsComponent.leftCollided && !physicsComponent.rightCollided) {
					physicsComponent.scale.X = 0.5f;
					physicsComponent.scale.Y = 1.5f;
					physicsComponent.vy = jumpHeight;
					doubleJumped = true;
				}
				// Variable jumping
			} else if (kDown && !physicsComponent.bottomCollided) {
				physicsComponent.vy = -jumpHeight*2;
			}
			else if (kJumpRelease) { 
				if (physicsComponent.vy > 0)
					physicsComponent.vy *= 0.25f;
			}

			// Jump state check
			if (!physicsComponent.bottomCollided) {
				state = JUMP;

				if (physicsComponent.leftCollided)
					facing = 1;
				if (physicsComponent.rightCollided)
					facing = -1;
			}
			physicsComponent.scale.X = Utils.Approach(physicsComponent.scale.X, defaultScale, 0.05f);
			physicsComponent.scale.Y = Utils.Approach(physicsComponent.scale.Y, defaultScale, 0.05f);
			//vx *=  Time.deltaTime * Settings.Speed;
			//vy *=  Time.deltaTime * Settings.Speed;

			if (image != null) {
				Vector2 s = new Vector2 (image.scale.X, image.scale.Y);
				s.X *= facing;
				image.scale = s;
			}

		}


	}

}

