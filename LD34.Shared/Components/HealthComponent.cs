﻿using System;
using Nez;
using Microsoft.Xna.Framework;
using Nez.Tweens;
using Nez.Sprites;

namespace LD34.Shared
{
	public class HealthComponent : Component
	{
		public int maxHealth;
		public int health;
		public bool stunned;
		public float stunTime = 1f;
		public float stunCounter;

		PhysicsComponent phy;

		public HealthComponent (int maxHealth) : base()
		{
			this.maxHealth = maxHealth;
			this.health = maxHealth;
		}

		public override void onAwake ()
		{
			base.onAwake ();
			phy = entity.getComponent<PhysicsComponent> ();
		}

		public override void update ()
		{
			base.update ();

			if (stunned) {
				stunCounter += Time.deltaTime;
				if (stunCounter >= stunTime) {
					stunned = false;
					stunCounter = 0;
				}
			}

		}

		public void Damage(int amount, Entity attacker = null, bool pushBack = false)
		{
			health -= amount;
			stunned = true;

			if (pushBack && attacker != null) {
				if (attacker.position.X < entity.position.X) {
					phy.vx = 10;
				} else {
					phy.vx = -10;
				}

				phy.vy = 10;
			}

			// If it's a friend, we kill it if the health reaches 0
			if (entity.name == "friend") {
				if (health <= 0) {
					BounceComponent bc = entity.getComponent<BounceComponent> ();
					if (bc != null) {
						entity.removeComponent (bc);
					}
					FleeComponent fc = entity.getComponent<FleeComponent> ();
					if (fc != null) {
						entity.removeComponent (fc);
					}
					PhysicsComponent pc = entity.getComponent<PhysicsComponent> ();
					InputComponent ic = entity.getComponent<InputComponent> ();
					Sprite image = entity.getComponent<Sprite> ();
					pc.vy = 5;
					pc.vx = 0;
					ic.left = false;
					ic.right = false;
					ic.jump = false;
					PropertyTweens.floatPropertyTo (image, "rotation", (float)Math.PI/2, 0.5f).start();
					entity.addComponent (new DelayedRemoveComponent (3f));
					MainScene.instance.hp -= 1;
				}
			}
		}
	}
}

