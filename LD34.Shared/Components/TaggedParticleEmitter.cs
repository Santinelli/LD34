﻿using System;
using Nez.Particles;

namespace LD34.Shared
{
	public class TaggedParticleEmitter : ParticleEmitter
	{
		public int tag;

		public TaggedParticleEmitter (ParticleEmitterConfig config, bool start, int tag) : base(config, start)
		{
			this.tag = tag;
		}
	}
}

