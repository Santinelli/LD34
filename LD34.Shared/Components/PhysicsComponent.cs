﻿using System;
using Nez;
using Microsoft.Xna.Framework;
using Nez.Sprites;

namespace LD34.Shared
{
	public class PhysicsComponent : Component
	{
		/// <summary>
		/// Cached Image component
		/// </summary>
		public Sprite image;

		// Velocity
		public float vx = 0;
		public float vy = 0;

		// Used for sub-pixel movement
		public float cx = 0;
		public float cy = 0;

		public bool onGroundPrev = false;
		public bool onLeftPrev = false;
		public bool onRightPrev = false;
		public bool onTopPrev = false;

		// Position
		public Vector2 pos = new Vector2();

		// For squash + stretch
		public Vector2 scale = new Vector2(1, 1);

		public bool bottomCollided = false;
		public bool leftCollided = false;
		public bool topCollided = false;
		public bool rightCollided = false;

		public int layerMask;

		public float width = 0;
		public float height = 0;

		Sprite img;
		MovementComponent move;

		public PhysicsComponent () : base()
		{
		}

		public override void onAwake ()
		{
			base.onAwake ();
			pos = entity.position;
			img = entity.getComponent<Sprite> ();
			if (img != null) {
				image = img;
				width = img.width;
				height = img.height;
			}

		}

		public override void update ()
		{
			base.update ();

			if (img != null) {
				width = img.width;
				height = img.height;
			}

			move = entity.getComponent<MovementComponent> ();
			if (move != null) {
				width *= move.defaultScale;
				height *= move.defaultScale;
			}

			pos = entity.position;

			// End Step
			// Handle sub-pixel movement
			cx += vx * Time.deltaTime * 45;
			cy += vy * Time.deltaTime * 45;

			//distanza di movimento se non c'è nessun ostacolo
			int vxNew = Mathf.roundToInt(cx);
			int vyNew = Mathf.roundToInt(cy);

			//differenza da sommare al prossimo frame
			//Log::log("---------------------vy "+std::to_string(Game::deltaTime));
			cx -= vxNew;
			cy -= vyNew;

			//movimento l'ogggetto in Y  pixel per pixel fino al termine della velocità o
			//fino alla collisione con un altro oggetto che non si può muovere
			if (Math.Abs(vyNew) != 0)
			{
				bool top = (vyNew <= 0.0f) ? false : true;
				int vyABS = Math.Abs(vyNew);
				for (int i = 0; i < vyABS; i++)
				{
					bool collided = PixelCollisions.Collide(pos.X, pos.Y, width, height, top? DirectionUnit.Top : DirectionUnit.Bottom, layerMask);
					if(collided){
						vy = 0;
						break;
					}else{
						pos.Y += (top?-1:1);
					}
				}
			}
			//ripeto la stessa procedura fatta sulla Y
			if (Math.Abs(vxNew) != 0)
			{
				bool right = (vxNew >= 0.0f) ? true : false;
				int vxABS = Math.Abs(vxNew);
				for (int i = 0; i < vxABS ; i++)
				{
					bool collided = PixelCollisions.Collide(pos.X, pos.Y, width, height, right? DirectionUnit.Right : DirectionUnit.Left, layerMask);

					if(collided){
						vx = 0;
						break;
					}else{
						pos.X += right?1:-1;
					}
				}
			}

			entity.position = pos;

			if (image != null) {
				image.scale = scale;
			}

			onGroundPrev = bottomCollided;
			onTopPrev = topCollided;
			onLeftPrev = leftCollided;
			onRightPrev = rightCollided;
			bottomCollided = PixelCollisions.Collide(entity.position.X,entity.position.Y,width,height,DirectionUnit.Bottom,layerMask);
			leftCollided = PixelCollisions.Collide(entity.position.X,entity.position.Y,width,height,DirectionUnit.Left,layerMask);
			topCollided =  PixelCollisions.Collide(entity.position.X,entity.position.Y,width,height,DirectionUnit.Top,layerMask);
			rightCollided = PixelCollisions.Collide(entity.position.X,entity.position.Y,width,height,DirectionUnit.Right,layerMask);

		}
	}
}

