﻿using System;
using Nez;
using Microsoft.Xna.Framework;
using Nez.Tweens;
using Nez.Sprites;

namespace LD34.Shared
{
	public class ScrollingTextComponent : Component
	{
		//start and end position. set in start method
		private Vector2 start;
		private Vector2 end;

		//animation curves for movement along path and alpha
		public EaseType acMove;
		public EaseType acAlpha;

		//time to animate in seconds
		public float time = 2.0f;

		//distance to move for animation. Speed is distance/time for units per second.
		//a longer time or shorter distance will slow the movement speed down
		public float distance = 20.0f;

		//the end point is moved +/- this amount left or right at random.
		//this is just so the text moves upwards at an angle and not always right on top of each other
		public float xOffset = 0.5f;

		public string text;

		Text textComponent;

		public ScrollingTextComponent (string text) : base()
		{
			this.text = text;
		}

		public override void onAwake ()
		{
			base.onAwake ();

			textComponent = entity.getComponent<Text> ();
			textComponent.text = text;

			//starting position
			start = entity.position;
			//random end position above starting position
			end = start + new Vector2(Nez.Random.range(-xOffset, xOffset), -distance);

			//start the animation
			PropertyTweens.vector2PropertyTo(entity, "position", end, time).setEaseType(EaseType.SineInOut).start();
			PropertyTweens.colorPropertyTo (textComponent, "color", Color.Transparent, time).setEaseType(EaseType.SineInOut).start ();
		}
	}
}

