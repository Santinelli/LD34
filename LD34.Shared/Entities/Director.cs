﻿using System;
using System.Collections.Generic;
using System.Linq;

using Nez;
using Nez.Tweens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Nez.Sprites;

namespace LD34.Shared
{
	public class Director : Entity
	{
		public Director () : base()
		{
		}

		public void Reset() 
		{
			MainScene.instance.score = 0;
			MainScene.instance.hp = 10;
		}

		public override void onAwake ()
		{
			base.onAwake ();
			Reset ();
		}

		public override void update ()
		{
			base.update ();

			MainScene.instance.cursor.position = MainScene.instance.guiCamera.screenToWorldPoint (Input.mousePosition);

			if (Game1.gameOver)
				return;

			switch (MainScene.instance.gameState) {
			case MainScene.GameState.Intro:
				{
					MainScene.instance.introScreen.enabled = true;
					if (Input.leftMouseButtonPressed) {
						MainScene.instance.gameState = MainScene.GameState.Play;
						MainScene.instance.Restart ();
					}
					break;
				}
			case MainScene.GameState.Play:
				{
					MainScene.instance.introScreen.enabled = false;
					MainScene.instance.hpLabel.enabled = true;
					MainScene.instance.scoreLabel.enabled = true;
					break;
				}
			case MainScene.GameState.GameOver:
				{
					break;
				}
			}

			// Update GUI
			MainScene.instance.hpLabel.text = "HP: " + MainScene.instance.hp;
			MainScene.instance.scoreLabel.text = "SCORE: " + MainScene.instance.score;

			// Main gameplay
			if (Input.leftMouseButtonPressed) {
				Vector2 mousePos = scene.camera.screenToWorldPoint (Input.mousePosition);
				List<Entity> enemies = scene.entities.entitiesWithTag ((int)EntityTag.Enemy);
				foreach (var enemy in enemies) {
					Sprite image = enemy.getComponent<Sprite> ();
					ChaseComponent chase = enemy.getComponent<ChaseComponent> ();
					Rectangle bounds = enemy.collider.bounds;
					if (bounds.Contains (mousePos) && chase != null) {
						PhysicsComponent pc = enemy.getComponent<PhysicsComponent> ();
						InputComponent ic = enemy.getComponent<InputComponent> ();
						TaggedParticleEmitter pe = enemy.components.OfType<TaggedParticleEmitter> ().FirstOrDefault(f => f.tag == (int)ParticleTag.BlockExplo);
						if (pe != null) {
							pe.emit (10);
						}

						pc.vy = 5;
						pc.vx = 0;
						ic.left = false;
						ic.right = false;
						enemy.removeComponent (chase);
						PropertyTweens.floatPropertyTo (image, "rotation", (float)Math.PI/2, 0.5f).start();
						PropertyTweens.vector2PropertyTo (enemy, "position", new Vector2(enemy.position.X, enemy.position.Y+16), 2f).start();
						enemy.addComponent (new DelayedRemoveComponent (3f));
						MainScene.instance.score += 1;
						SoundEffect se = MainScene.instance.sfxForks [Nez.Random.range (0, MainScene.instance.sfxForks.Count)];
						MainScene.instance.audioManager.Play (se);
						EntityFactory.CreateScrollingText (MainScene.instance, enemy.position.X, enemy.position.Y-8, "+1");
						MainScene.instance.flashingSpritesRenderer.add (image);
					}
				}
			}

			// GameOver check
			if (MainScene.instance.hp <= 0) {
				MainScene.instance.friendsSpawner.Stop ();
				MainScene.instance.enemiesSpawner.Stop ();
				Game1.ShowGameOver ();
			}

		}

	}
}

