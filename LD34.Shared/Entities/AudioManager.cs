﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework.Audio;

namespace LD34.Shared
{
	public class AudioManager
	{
		public static AudioManager instance;
		public List<SoundEffectInstance> effects = new List<SoundEffectInstance>();
		public int MAX_INSTANCES = 16;
		public int lastEffectPlayed;

		public AudioManager ()
		{
			instance = this;
			for (int i = 0; i < MAX_INSTANCES; i++) {
				effects.Add (null);
			}
		}

		public void Play(SoundEffect se) {
			lastEffectPlayed++;
			if (lastEffectPlayed >= MAX_INSTANCES) {
				lastEffectPlayed = 0;
			}
			SoundEffectInstance sei = effects [lastEffectPlayed];
			if (sei != null) {
				sei.Stop ();
				sei.Dispose ();
			}
			sei = se.CreateInstance ();
			sei.Play ();
			effects [lastEffectPlayed] = sei;
		}
	}
}

