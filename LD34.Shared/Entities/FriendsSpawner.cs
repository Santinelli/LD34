﻿using System;
using System.Collections;
using System.Collections.Generic;
using Nez;

namespace LD34.Shared
{
	public class FriendsSpawner : Entity
	{
		public float startSpawnRate = 5f;
		public float spawnRate;

		public float spawnCooldown;

		public List<Entity> spawners = new List<Entity>();

		public bool paused = true;

		public int totSpawned;
		public int spawnedChange = 5;
		public float spawnIncrease = 0.95f;

		public FriendsSpawner () : base()
		{
		}

		public override void onAwake ()
		{
			base.onAwake ();
			Reset ();
		}

		public void Reset()
		{
			spawnRate = startSpawnRate;
			spawnCooldown = 0;
			spawners.Clear();
			spawners.AddRange(MainScene.instance.entities.entitiesWithTag ((int)EntityTag.FriendSpawner));
			totSpawned = 0;
		}

		public override void update ()
		{
			base.update ();
			if (paused)
				return;

			if (spawners.Count == 0)
				return;
			spawnCooldown -= Time.deltaTime;
			if (spawnCooldown <= 0) {
				SpawnFriend ();
				spawnCooldown = spawnRate;
			}
		}

		public void SpawnFriend()
		{
			Entity spawner = spawners [Nez.Random.range(0, spawners.Count)];
			EntityFactory.CreateFriend (MainScene.instance, spawner.position.X+8, spawner.position.Y+8);
			if (totSpawned % spawnedChange == 0 && spawnRate >= 1f) {
				spawnRate *= spawnIncrease;
			}
			totSpawned++;
		}

		public void Stop() {
			paused = true;
		}

		public void Start() {
			paused = false;
		}
	}
}

